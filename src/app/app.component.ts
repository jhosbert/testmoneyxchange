import { Component, OnDestroy, OnChanges, HostListener } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Subscription } from 'rxjs/Subscription';
import { AuthService } from './auth/shared/auth.service';
import { MessagesService } from './shared/messages/messages.service';
import { Router, NavigationEnd } from '@angular/router';
import { User } from './users/shared/user.model';
import { Observable } from 'rxjs/Observable';
import { GoogleAnalyticsEventsService } from './google-analytics-events.service';
import { trigger, style, animate, transition } from '@angular/animations';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [AuthService],
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('sideBar', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('200ms', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ opacity: 1 }),
        animate('200ms', style({ opacity: 0 }))
      ])
    ]),
    trigger('page', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('200ms 200ms', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ opacity: 1 }),
        animate('200ms 200ms', style({ opacity: 0 }))
      ])
    ])
  ]
})
export class AppComponent implements OnDestroy {
  title = 'app';
  public message: any;
  public subscription: Subscription;
  public userName: String;
  public isLarge: boolean;
  public sidebar: boolean = false;

  constructor(
    private authService: AuthService,
    private messageService: MessagesService,
    public router: Router,
    public googleAnalyticsEventsService: GoogleAnalyticsEventsService
  ) {
    this.message = {};
    
  }


  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.isLarge = window.innerWidth >= 1150;
  }

  ngOnInit() {
    this.isLarge = window.innerWidth >= 1150;
    
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }


}
