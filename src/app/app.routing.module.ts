import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LandingComponent } from './auth/landing/landing.component';


const routes: Routes = [
  { path: '', component: LandingComponent},
  {  path: 'landing', loadChildren: 'app/auth/auth.module#AuthModule' },
  {  path: '**', redirectTo: '/landing' }
]


@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
	providers: []
})

export class NgPlayRoutingModule { }
