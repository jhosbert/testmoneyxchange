import { Injectable } from '@angular/core';
import { Http, ResponseContentType } from '@angular/http';
import * as FileSaver from 'file-saver';
import { environment } from '../../../environments/environment';
import { Router }      from '@angular/router';
import { User } from '../../users/shared/user.model';

const API_URL = environment.api_url;

@Injectable()
export class Utility {
    public scWidth: number;

    constructor(
      private http: Http,
    ) { }

    public newGuid() {
      return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
          var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
          return v.toString(16);
      });
  }
}
