import { NgModule }      from '@angular/core';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { CommonModule }  from '@angular/common'; 
import { SuiModule, SuiDatepickerModule, SuiCheckboxModule, SuiRatingModule} from 'ng2-semantic-ui';

import { AuthRoutingModule }   from './auth-routing.module';

import { AuthService } from './shared/auth.service';
import { AuthGuardService } from "./shared/auth-guard.service";
import { LoginGuardService } from "./shared/login-guard.service";
import { LandingComponent } from './landing/landing.component';
import { NouisliderModule } from 'ng2-nouislider';
import { NgxLoadingModule } from 'ngx-loading';

import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import {MatSliderModule} from '@angular/material/slider'
import { CurrencyMaskModule } from "ng2-currency-mask";


@NgModule({
  imports: [ 
    AuthRoutingModule,
    SuiModule, SuiDatepickerModule, SuiCheckboxModule, SuiRatingModule,
    FormsModule,
    ReactiveFormsModule,
    NouisliderModule,
    CommonModule,
    MatButtonModule, 
    MatCheckboxModule,
    MatSliderModule,
    CurrencyMaskModule,
    NgxLoadingModule
  ],
  providers: [ 
    AuthService,
    AuthGuardService,
    LoginGuardService,
    
  ],
  declarations: [
    LandingComponent,
  ]
  
})
export class AuthModule { }
