import { Injectable } from '@angular/core';
import { Request, XHRBackend, RequestOptions, Response, Http, RequestOptionsArgs, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { error } from 'util';

@Injectable()
export class ExtendedHttpService extends Http {

  private ERROR_401: number = 401;
  private ERROR_403: number = 403;

  constructor(
    backend: XHRBackend,
    defaultOptions: RequestOptions,
    private router: Router) {

      super(backend, defaultOptions);

  }

  request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    //do whatever 
    if (typeof url === 'string') {
      if (!options) {
        options = { headers: new Headers() };
      }
      this.setHeaders(options);

    } else {
      this.setHeaders(url);
    }

    return super.request(url, options)
      .map((res)=> {
        if(res.headers.get("token")){
          return res;
        }

        return res;
      }
    ).catch((err) => {
      let unathorized_call = err.status === this.ERROR_401 || err.status === this.ERROR_403;
      
      return Observable.throw(err);
    });
  }

  private catchErrors(res) {
    return (res: Response) => {

      let unathorized_call = res.status === this.ERROR_401 || res.status === this.ERROR_403;
      if (unathorized_call) {
        //handle authorization errors
        return Observable.empty<Response>();

      }
      return Observable.throw(res);
    };
  }

  private setHeaders(objectToSetHeadersTo: Request | RequestOptionsArgs) {
    //add whatever header that you need to every request
    //in this example I add header token by using authService that I've created
  }

}
