import { Component, OnInit, EventEmitter, HostListener } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../shared/auth.service';
import { trigger, style, animate, transition } from '@angular/animations';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';


@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
  animations: [
    trigger('sideBar', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('200ms', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ opacity: 1 }),
        animate('200ms', style({ opacity: 0 }))
      ])
    ]),
    trigger('page', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('200ms 200ms', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ opacity: 1 }),
        animate('200ms 200ms', style({ opacity: 0 }))
      ])
    ])
  ]
})
export class LandingComponent implements OnInit {
  public isLarge: boolean;
  public value: number;
  public result: number = 0.0000;
  public loading: boolean = false;

  constructor(
    private router: Router,
    private authService: AuthService,
    private activatedRoute: ActivatedRoute,
    ) {
      
    }

  public request(){
    this.router.navigate(['/auth/request']);
  }


  ngOnInit() {
    this.isLarge = window.innerWidth >= 1150;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.isLarge = window.innerWidth >= 1150;
  }

  public price(){
    this.loading = true;
    this.authService.get_price().subscribe(
      (response : any) => {
        this.loading = false;
        this.result = this.calculate(this.value, response.rates.USD)
        this.retry();
      },
      error => {
        this.loading = false;
        alert("Error tomando la tasa del dia, Intente de nuevo")
      }
    );
  }

  public calculate(value: number, change: number): number{
    let res = value * change;
    return res
  }

  public retry(){
    setTimeout(()=>{   
      this.loading = true;
      this.authService.get_price().subscribe(
        (response: any) => {
          this.loading = false;
          this.result = this.calculate(this.value, response.rates.USD)
          this.retry();
        },
        error => {
          this.loading = false;
          alert("Error tomando la tasa del dia, Intente de nuevo")
        }
      );
    }, 1000000);
  }

}
