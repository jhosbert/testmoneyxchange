import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingComponent } from './landing.component';

import { FormsModule } from '@angular/forms';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { NgxLoadingModule } from 'ngx-loading';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from '../shared/auth.service';
import { HttpModule } from '@angular/http';



describe('LandingComponent', () => {
  let component: LandingComponent;
  let fixture: ComponentFixture<LandingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule,CurrencyMaskModule, NgxLoadingModule, RouterTestingModule, HttpModule ],
      declarations: [ LandingComponent ],
      providers: [ AuthService ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should calculate today price', () => {
    let value = 1;
    let change = 1.136667
    
    expect(component.calculate(value,change)).toBe(1.136667);
  });
});
