import { NgModule }     from '@angular/core';
import { Routes,
         RouterModule } from '@angular/router';

import { LandingComponent } from './landing/landing.component'

const routes: Routes = [
  { path: '',
    // component: LoginComponent,
    children: [
      {  path: '', component: LandingComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {}


