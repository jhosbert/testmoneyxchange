import { Injectable } from '@angular/core';
import {
  Headers,
  Http,
  Response,
  URLSearchParams,
  RequestOptions
} from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';

import 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { BehaviorSubject } from 'rxjs/Rx';

import { User } from '../../users/shared/user.model';
import { element } from 'protractor';

const API_URL = environment.api_url;

@Injectable()
export class AuthService {
  

  constructor(private http: Http, private router: Router) {
  }

  public get_price(): Observable<User> {
    return this.http.get(`${API_URL}/api/latest?access_key=e6375b0ac6f7bb52859ed163c5d609bf`).map(response => {
      let res = response.json();
      return res;
    });
  }


  public handleError(error: Response) {
    if (error.json().non_field_errors)
      throw Observable.throw(error.json().non_field_errors.pop());

    Object.keys(error.json()).forEach(key => {
      let errorMsg = error.json()[key].pop();
      let errorResponse = `${key}: ${errorMsg}`;
      throw Observable.throw(errorResponse);
    });

    return Observable.throw(error.json());
  }

  public handleErrorHiddenField(error: Response): ErrorObservable {
    if (error.json().non_field_errors)
      throw Observable.throw(error.json().non_field_errors.pop());

    Object.keys(error.json()).forEach(key => {
      let errorMsg = error.json()[key].pop();
      let errorResponse = `${key}: ${errorMsg}`;
      throw Observable.throw(errorResponse);
    });

    return Observable.throw(error.json());
  }
}
